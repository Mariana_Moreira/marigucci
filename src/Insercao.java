
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author aluno
 */
public class Insercao {
    public void inserirProduto(String Codigo, String Tipo, String Tamanho, String Valor, String Quantidade, String Genero){
        try {
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("INSERT INTO sc_marigucci.Produto(\"código\", Tipo, Tamanho, Valor, Quantidade, \"gênero\")VALUES(?,?,?,?,?,?)");
            ps.setInt(1, Integer.valueOf(Codigo));
            ps.setString(2,Tipo);
            ps.setString(3, Tamanho);
            ps.setInt(4,Integer.valueOf(Valor));
            ps.setInt(5,Integer.valueOf(Quantidade));
            ps.setString(6,Genero);
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void inserirEstoque(String Prateleira, String Setor, String Andar, String Codigo){
        try {
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("INSERT INTO sc_marigucci.Estoque(Prateleira, Setor, Andar, \"código\") VALUES (?,?,?,?)");
            ps.setInt(1, Integer.valueOf(Prateleira));
            ps.setString(2, Setor);
            ps.setInt(3, Integer.valueOf(Andar));
            ps.setInt(4, Integer.valueOf(Codigo));
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
    
    public void inserirFornecedor(int codFornecedor, String contrato, String nome, int codigo){
        try {
            Connection c = Conexao.obterConexao();
            String SQL = "INSERT INTO sc_marigucci.fornecedor(codfornecedor,contrato, nomefornecedor, \"código\") VALUES (?,?,?,?)";
            PreparedStatement s = c.prepareStatement(SQL);
            s.setInt(1, codFornecedor);
            s.setString(2, contrato);
            s.setString(3, nome);
            s.setInt(4, codigo);
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
}
