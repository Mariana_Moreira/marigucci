
public class Prateleira {
    private int numPrateleira;

    public Prateleira(int numPrateleira) {
        this.numPrateleira = numPrateleira;
    }

    public int getNumPrateleira() {
        return numPrateleira;
    }

    public void setNumPrateleira(int numPrateleira) {
        this.numPrateleira = numPrateleira;
    }

    @Override
    public String toString() {
        return  "" + numPrateleira + '}';
    }
    
    
    
    
    
}
