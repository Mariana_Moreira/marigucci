
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author aluno
 */
public class ListarEstoqueDeRoupas {
    public void listar(JList listProduto){
        try {
            listProduto.removeAll();
            DefaultListModel dfm = new DefaultListModel();
            Connection c = Conexao.obterConexao();
            String SQL = "SELECT * FROM sc_marigucci.Produto";
            PreparedStatement ps = c.prepareStatement (SQL);//Criar instrução
            ResultSet rs = ps.executeQuery(SQL);//Executar instrução
            while(rs.next()){
                dfm.addElement(rs.getString("Código"));
            }
            listProduto.setModel(dfm);
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(ListarEstoqueDeRoupas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void lista (JList listEstoque){
        try {
            listEstoque.removeAll();
            DefaultListModel dfm = new DefaultListModel();
            Connection c = Conexao.obterConexao();
            String SQL = "SELECT * FROM sc_marigucci.Estoque";
            Statement ps = c.createStatement();
            ResultSet rs = ps.executeQuery(SQL);
            while(rs.next()){
                dfm.addElement(rs.getString("Setor")+" - "+rs.getString("Andar"));
            }
            listEstoque.setModel(dfm);
            c.close();
            } catch (SQLException ex) {
            Logger.getLogger(ListarEstoqueDeRoupas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void selecaoPrateleira(JComboBox combo){
        try {
            DefaultComboBoxModel m = new DefaultComboBoxModel();
            Connection c = Conexao.obterConexao();
            PreparedStatement p = c.prepareStatement("SELECT * FROM sc_marigucci.Estoque");
            ResultSet rs = p.executeQuery();
            while(rs.next()){
                Prateleira pb = new Prateleira(rs.getInt("prateleira"));
                m.addElement(pb);
            }
            combo.setModel(m);
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(ListarEstoqueDeRoupas.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
}
