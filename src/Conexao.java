
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Conexao {
    public static Connection obterConexao(){
        try {
            Class.forName("org.postgresql.Driver");
            String usuario = "mariana_moreira";
            String senha = "mariana";
            String banco = "jdbc:postgresql://10.90.24.54/mariana";
            return DriverManager.getConnection(banco, usuario, senha);
        } catch (SQLException ex) {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    public static void main(String[] args) {
        Conexao.obterConexao();
    }
}
